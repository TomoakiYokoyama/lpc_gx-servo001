
#include "LPC11xx.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "flash.h"
#include "memmap.h"


int	 srcbuf[PAGE_SIZE/sizeof(int)];

typedef void (*IAP)(uint32_t [], uint32_t []);
IAP iap_entry = (IAP)0x1fff1ff1;

uint32_t command[5], result[4];


static int call_IAP(int cmd,int arg1,int arg2,int arg3,int arg4)
{
	/* Send Reinvoke ISP command to ISP entry point*/
	command[0] = cmd;
	command[1] = arg1;
	command[2] = arg2;
	command[3] = arg3;
	command[4] = arg4;

	iap_entry(command, result);

	return result[0];
}

static void erase_buf(void)
{
	int i;
	for(i=0; i< (PAGE_SIZE/sizeof(int) ); i++) {
		srcbuf[i]=(-1);
	}
}


void FLASH_ProgramPage(int adr)
{
	//セクタ4094bytesを消すけど256bytesのページしか書き込まないソースですよん。
	//気にせず使えるかもしれんけど
	int page;
	int sector;
	int rc;
	int dst;;
	page = (adr >> PAGE_SHIFT) & PAGE_MASK ;
	sector = (adr >> SECTOR_SHIFT) & SECTOR_MASK ;
	dst = page * PAGE_SIZE;

	rc = call_IAP(IAP_PREPARE,sector,sector	  ,CLOCK_KHz,0);
	rc = call_IAP(IAP_RAM2FLASH ,dst,(int) srcbuf , PAGE_SIZE ,CLOCK_KHz);

	erase_buf();
}

void FLASH_EraseSector(int adr)
{
	int sector;
	int rc;
	sector = (adr >> SECTOR_SHIFT) & SECTOR_MASK ;
	rc = call_IAP(IAP_PREPARE,sector,sector	  ,CLOCK_KHz,0);

	rc = call_IAP(IAP_ERASE  ,sector,sector	  ,CLOCK_KHz,0);
}

void FLASH_ReadPage(int adr)
{
	int* addrt;
	int i;

	for(i = 0;i < (PAGE_SIZE/sizeof(int));i++){
		srcbuf[i] = 0xFFFFFFFF;
	}

	addrt = (int*)((unsigned int)adr & 0xFFFFF000);
	for(i = 0;i < (PAGE_SIZE/sizeof(int));i++){
		srcbuf[i] = addrt[i];
	}
}

void FLASH_WRITE_MEMMAP(){
	int i,j;
	FLASH_EraseSector((int)constmemmap);
	erase_buf();
	for(i = 0; i < 4 ; i++){
		for(j = 0; j < (MEMMAP_SIZE/sizeof(uint32_t));j++ ){
			srcbuf[j + (i*MEMMAP_SIZE/sizeof(uint32_t))] = memmap[i].u32[j];
		}
	}
	FLASH_ProgramPage((int)constmemmap);
}

void FLASH_WRITE_MEMMAP_ID(uint8_t id){
	int i,j;
	erase_buf();
	for(j = 0; j < PAGE_SIZE/sizeof(int);j ++ ){
		srcbuf[j] = ((int*)(constmemmap))[j];
	}
	FLASH_EraseSector((int)constmemmap);

	for(i = 0; i < 4 ;i++){
		if(memmap[i].map.Servo_ID == id){
			for(j = 0; j < (MEMMAP_SIZE/sizeof(uint32_t));j++ ){
				srcbuf[j + (i*MEMMAP_SIZE/sizeof(uint32_t))] = memmap[i].u32[j];
			}
			break;
		}
	}
	FLASH_ProgramPage((int)constmemmap);
}


void FLASH_READ_MEMMAP(){
	int i,j;
	for(i = 0; i < 4 ;i++){
		for(j = 0; j < MEMMAP_SIZE;j ++ ){
			memmap[i].u8[j] = ((uint8_t*)(constmemmap))[i*MEMMAP_SIZE + j];
		}
		memmap[i].map.Goal_Position = 0;
		memmap[i].map.Goal_Time = 0;
		memmap[i].map.Torque_Enable = 0;
	}
}

