/*
 * rs30x.c
 *
 *  Created on: 2014/10/01
 *      Author: yokoyama
 */
#include "LPC11xx.h"
#include "rs30x.h"
#include "uart.h"
#include "memmap.h"

RS30xPACKS rcvpack;
volatile int rcvpackcnt;
volatile int rcvpacklength;

void SetUartTx(){
	  LPC_IOCON->PIO1_6 &= ~0x07;    /*  UART I/O config */
	  LPC_IOCON->PIO1_6 |= 0x00;     /* UART RXD */

	  LPC_IOCON->PIO1_7 &= ~0x07;
	  LPC_IOCON->PIO1_7 |= 0x01;     /* UART TXD */
}

void SetUartRx(){
	while((LPC_UART->LSR & (LSR_TEMT | LSR_THRE)) != (LSR_TEMT | LSR_THRE)){
		__NOP();
	}
	  rcvpackcnt = 0;

	  LPC_IOCON->PIO1_6 &= ~0x07;    /*  UART I/O config */
	  LPC_IOCON->PIO1_6 |= 0x01;     /* UART RXD */

	  LPC_IOCON->PIO1_7 &= ~0x07;
	  LPC_IOCON->PIO1_7 |= 0x00;     /* UART TXD */
}

uint8_t getCeackSum(RS30xPACKS pack,int lenght){
	int i;
	uint8_t sum = pack.pack.ID;
	for(i = 3;i < lenght-1;i++){
		sum = sum^pack.u8[i];
	}
	return sum;
}

void proc_Rs30x(){
	/* Loop forever */
	while(UARTReadPos != UARTCount){
		rcvpack.u8[rcvpackcnt] = UARTBuffer[UARTReadPos];
		rcvpackcnt++;
		UARTReadPos ++;
		UARTReadPos %= UARTBUFSIZE;

		if(rcvpackcnt == RS30xRCV_Header_H){
			if(rcvpack.pack.Header_H != 0xFA){
				rcvpackcnt = 0;
			}
		}
		else if(rcvpackcnt == RS30xRCV_Header_L){
			if(rcvpack.pack.Header_L != 0xAF){
				rcvpackcnt = 0;
			}
		}
		else if(rcvpackcnt == RS30xRCV_Count){
			if((rcvpack.pack.Flag & RS30xFLAG_RET_MASK) != 0){ //return pack
				rcvpacklength = 8;
			}
			else{//not return pack
				rcvpacklength = 8 + (rcvpack.pack.Count * rcvpack.pack.Length);
			}
			if(rcvpacklength < 0 || rcvpacklength > 0x500){
				rcvpacklength = 0;
				rcvpackcnt = 0;
			}
		}
		else if((rcvpackcnt > (int)RS30xRCV_Count) && (rcvpackcnt >= rcvpacklength)){
			if(rcvpacklength > 255){
				rcvpacklength = 0;
				rcvpackcnt = 0;
			}
			if(getCeackSum(rcvpack,rcvpacklength) != rcvpack.u8[rcvpacklength - 1]){
				rcvpackcnt = 0;
				rcvpacklength = 0;
			}
			else{
				if((rcvpack.pack.Flag & RS30xFLAG_RET_MASK) != 0){ //return pack
					uint8_t addr = 0;
					uint8_t size = 0;

					switch(rcvpack.pack.Flag & RS30xFLAG_RET_MASK){
					case RS30xFLAG_RET_SET_ADDR:
						addr = rcvpack.pack.Address;
						size = rcvpack.pack.Length;
						Rs30xReturnPack(rcvpack.pack.ID,addr,size);
						break;
					case RS30xFLAG_RET_ACK:
						SetUartTx();
						{
							uint8_t data = 0x07;
							UARTSend(&data ,1 );
						}
						SetUartRx();
						break;
					case RS30xFLAG_RET_00to29:
						addr = 0;
						size = 30;
						Rs30xReturnPack(rcvpack.pack.ID,addr,size);
						break;
					case RS30xFLAG_RET_20to29:
						addr = 20;
						size = 10;
						Rs30xReturnPack(rcvpack.pack.ID,addr,size);
						break;
					case RS30xFLAG_RET_30to41:
						addr = 30;
						size = 12;
						Rs30xReturnPack(rcvpack.pack.ID,addr,size);
						break;
					case RS30xFLAG_RET_30to59:
						addr = 30;
						size = 30;
						Rs30xReturnPack(rcvpack.pack.ID,addr,size);
						break;
					case RS30xFLAG_RET_42to59:
						addr = 42;
						size = 18;
						Rs30xReturnPack(rcvpack.pack.ID,addr,size);
						break;
					case RS30xFLAG_RET_60to127:
						addr = 60;
						size = 68;
						Rs30xReturnPack(rcvpack.pack.ID,addr,size);
						break;
					}
				}
				else{	//write
					if((rcvpack.pack.Flag & ~RS30xFLAG_RET_MASK) != 0){
						if((rcvpack.pack.Flag & RS30xFLAG_FLASH) != 0){
							if(rcvpack.pack.Address == 0xFF
									&& rcvpack.pack.Length == 0x0
									&& rcvpack.pack.Count == 0x0){
								__disable_irq();
								FLASH_WRITE_MEMMAP_ID(rcvpack.pack.ID);
								__enable_irq();
							}
						}
						if((rcvpack.pack.Flag & RS30xFLAG_INIT) != 0){
							if(rcvpack.pack.Address == 0xFF
									&& rcvpack.pack.Length == 0xFF
									&& rcvpack.pack.Count == 0x0)
								InitMemmapId(rcvpack.pack.ID);
						}
						if((rcvpack.pack.Flag & RS30xFLAG_REBOOT) != 0){
							if(rcvpack.pack.Address == 0xFF
									&& rcvpack.pack.Length == 0x0
									&& rcvpack.pack.Count == 0x0)
								NVIC_SystemReset();
						}
					}
					else{
						Rs30xWriteProc();
					}
				}

				rcvpacklength = 0;
				rcvpackcnt = 0;
			}
		}
	}


#if 0
	/* Wrap value back around */
	Set_UART_Tx();
	if (Chip_UART_SendRB(LPC_USART, &txring, (const uint8_t *) &key, 1) != 1) {
	}
	Set_UART_Rx();
#endif
}

uint8_t Rs30xFoundID(uint8_t id){
	int i;
	for(i = 0; i < 4 ; i ++){
		if(memmap[i].map.Servo_ID == id){
			return 1;
		}
	}
	return 0;
}

void Rs30xWriteMem(uint8_t servonum,uint8_t addr , uint8_t size,uint8_t* data){
	int i = 0;
	for(i = 0; i < size ; i ++){
		if((&memmap[servonum].u8[i + addr]) == (&memmap[servonum].map.Servo_ID)){
			if(Rs30xFoundID(data[i]) == 0){
				memmap[servonum].u8[i + addr] = data[i];
			}
		}
		else{
			memmap[servonum].u8[i + addr] = data[i];
		}
	}
}

void Rs30xWriteProc(){
	if(rcvpack.pack.ID == 0){	//Long
		int i = 0;
		int servonum;
		for(i = 0;i < rcvpack.pack.Count ;i++){
			uint8_t id = rcvpack.pack.Data[i*rcvpack.pack.Length];
			servonum = getServoNum(id);
			if(servonum >= 0){
				Rs30xWriteMem(servonum,rcvpack.pack.Address ,rcvpack.pack.Length - 1,(uint8_t*)(&(rcvpack.pack.Data[i*rcvpack.pack.Length + 1])));
			}
		}
	}
	else if(rcvpack.pack.ID == 0xFF){
		int i;
		for(i = 0;i < 4 ;i++){
			Rs30xWriteMem(i,rcvpack.pack.Address ,rcvpack.pack.Length,rcvpack.pack.Data);
		}
	}
	else{	//short
		int servonum = getServoNum(rcvpack.pack.ID);
		if(servonum < 0){
			return;
		}
		Rs30xWriteMem(servonum,rcvpack.pack.Address ,rcvpack.pack.Length,rcvpack.pack.Data);
	}
}


void Rs30xReturnPack(uint8_t id,uint8_t addr , uint8_t size){
	RS30xPACKS retpack;
	int retpacklength = 8;
	int i = 0;
	int servonum = getServoNum(id);
	if(servonum < 0){
		return;
	}

	retpack.pack.Header_H = 0xFD;
	retpack.pack.Header_L = 0xDF;
	retpack.pack.ID = id;
	retpack.pack.Flag = 0;
	retpack.pack.Address = addr;
	retpack.pack.Length = size;
	retpack.pack.Count = 1;

	for(i = 0; i < size ; i ++){
		retpack.pack.Data[i] = memmap[servonum].u8[i + addr];
	}
	retpacklength += i;

	retpack.pack.Data[i] = getCeackSum(retpack,retpacklength);

	for(i = 0;i < (((int)memmap[servonum].map.Return_Delay) * 125);i++){
		__NOP();
	}
	SetUartTx();
	UARTSend( retpack.u8,retpacklength );
	SetUartRx();
}
