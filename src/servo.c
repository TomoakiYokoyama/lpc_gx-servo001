#include "LPC11xx.h"
#include "uart.h"
#include "rs30x.h"
#include "config.h"
#include "gpio.h"
#include "ports.h"
#include "timer16.h"
#include "memmap.h"

#define SERVO_MTR_PWM_MAX (512)

const uint8_t servodio[4] = {
	P_MTR1_DIO,
	P_MTR2_DIO,
	P_MTR3_DIO,
	P_MTR4_DIO
};

const uint8_t servoout[4][2] = {
		{1,0},
		{0,1},
		{1,0},
		{0,1}
};

volatile short b_softstart[4] = {
		0,0,0,0
};
volatile short move_start_pos[4] = {
		0,0,0,0
};
volatile short last_write_goalpos[4] = {
		0,0,0,0
};
volatile short lastGoleTime[4] = {
		0,0,0,0
};
void initServo(){
	int i = 0;
	for(i = 0; i < 4;i ++){
		GPIOSetDir(servodio[i],GPIO_OUTPUT);
		b_softstart[i] = 0;
		move_start_pos[i] = 0;
		last_write_goalpos[i] = 0;
		lastGoleTime[i] = 0;
	}
	GPIOSetDir(P_MTR12_MODE,GPIO_OUTPUT);
	GPIOSetDir(P_MTR34_MODE,GPIO_OUTPUT);
	GPIOSetValue(P_MTR12_MODE,1);
	GPIOSetValue(P_MTR12_MODE,1);
	init_timer16B0(SERVO_MTR_PWM_MAX);
	enable_timer16(0);
	init_timer16B1(SERVO_MTR_PWM_MAX);
	enable_timer16(1);
}

void SetMotorPwm(){
	int i = 0;
	for(i = 0; i < 4;i ++){
		unsigned short pwm = SERVO_MTR_PWM_MAX;
		int out = memmap[i].map.Present_Speed;
		int maxtorque = (int)(SERVO_MTR_PWM_MAX * memmap[i].map.Max_Torque / 100.0);
		if(out > maxtorque)
			out = maxtorque - 1.0;
		else if(out < -maxtorque)
			out = -maxtorque + 1.0;

		if(out > 0){
			pwm = (SERVO_MTR_PWM_MAX - out);
		}
		else if(out < 0){
			pwm = (SERVO_MTR_PWM_MAX + out);
		}

		switch(i){
		case 0:
			LPC_TMR16B0->MR0 = pwm;
			break;
		case 1:
			LPC_TMR16B0->MR1 = pwm;
			break;
		case 2:
			LPC_TMR16B1->MR0 = pwm;
			break;
		case 3:
			LPC_TMR16B1->MR1 = pwm;
			break;
		}

		if(memmap[i].map.Torque_Enable == 0){
			GPIOSetValue(servodio[i],servoout[i][0]);
		}
		else if(memmap[i].map.Torque_Enable == 2){
			GPIOSetValue(servodio[i],servoout[i][0]);
		}
		else if(out > 0){
			GPIOSetValue(servodio[i],servoout[i][0]);
		}
		else if(out < 0){
			GPIOSetValue(servodio[i],servoout[i][1]);
		}
		else{
			GPIOSetValue(servodio[i],servoout[i][0]);
		}
	}
}



void ServoProc(){

	int i = 0;
	for(i = 0; i < 4;i ++){
		if(memmap[i].map.Torque_Enable != 1){
			memmap[i].map.Present_Speed = 0;
		}
		else{
			double out = memmap[i].map.Present_Posion;
			short goalpostemp = memmap[i].map.Goal_Position;

			if(goalpostemp > memmap[i].map.CW_Angle_Limit){
				goalpostemp = memmap[i].map.CW_Angle_Limit;
			}
			else if(goalpostemp < memmap[i].map.CCW_Angle_Limit){
				goalpostemp = memmap[i].map.CCW_Angle_Limit;
			}

			//スロースタート
			if(b_softstart[i] == 0){
				memmap[i].map.Goal_Time = 200;
				b_softstart[i] ++;
				move_start_pos[i] = memmap[i].map.Present_Posion;
				last_write_goalpos[i] = memmap[i].map.Present_Posion;
			}
			//スロースタート中で、補間完了したら
			else if(b_softstart[i] == 1 && (memmap[i].map.Goal_Time == memmap[i].map.Present_Time)){
				b_softstart[i] =2;
				memmap[i].map.Goal_Time = 0;
				 memmap[i].map.Present_Time = 0;
			}

			if(memmap[i].map.Goal_Time > 0){
				if(memmap[i].map.Present_Time == 0){
					move_start_pos[i] = memmap[i].map.Present_Posion;
				}
				if(last_write_goalpos[i] != goalpostemp){
					memmap[i].map.Present_Time = 0;
					move_start_pos[i] = memmap[i].map.Present_Posion;
					last_write_goalpos[i] = goalpostemp;
				}
				//補間中
				if(memmap[i].map.Goal_Time > memmap[i].map.Present_Time){
					out -= (move_start_pos[i] + (short)((double)(goalpostemp - move_start_pos[i]) * ((double)((int)memmap[i].map.Present_Time) / (int)memmap[i].map.Goal_Time)));
					memmap[i].map.Present_Time ++;
				}
				//補間終了
				else{
					out -= goalpostemp;
					last_write_goalpos[i] = goalpostemp;
					move_start_pos[i] = goalpostemp;
				}
			}
			else{
				out -= goalpostemp;
				memmap[i].map.Present_Time = 0;
				last_write_goalpos[i] = goalpostemp;
			}

			if(out < memmap[i].map.CW_Compliance_Margin && out > -memmap[i].map.CCW_Compliance_Margin){
				out = 0;
			}
			else{
				if(out > 0){	//cw
					out -= memmap[i].map.CW_Compliance_Margin;
					out *= 100.0/(memmap[i].map.CW_Compliance_Slope * 10.0);
					out += memmap[i].map.Punch/100.0;
				}
				else if(out < 0){
					out += memmap[i].map.CCW_Compliance_Margin;
					out *= 100.0/(memmap[i].map.CCW_Compliance_Slope * 10.0);
					out -= memmap[i].map.Punch/100.0;
				}
			}

			if(out > 100.0){
				out = 100.0;
			}
			else if(out < -100.0){
				out = -100.0;
			}
			if(memmap[i].map.Reverse == 0){
				memmap[i].map.Present_Speed = (short)(out * SERVO_MTR_PWM_MAX/100.0);
			}
			else{
				memmap[i].map.Present_Speed = (short)(-1 * out * SERVO_MTR_PWM_MAX/100.0);
			}

			if(memmap[i].map.Max_Torque > 100){
				memmap[i].map.Max_Torque = 100;
			}
		}
	}
	SetMotorPwm();
}
