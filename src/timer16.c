#include "LPC11xx.h"
#include "timer16.h"
#include "memmap.h"
#include "core_cm0.h"
#include "config.h"
#include "gpio.h"

extern char popflag;
extern char pophandflag;

extern uint16_t playSoundid;
extern uint16_t playSoundid_temp;
extern int32_t Soundcycle;
extern int32_t SoundChangeCycle;
extern double Soundoffset;
extern int32_t SoundCenter;
extern int32_t SoundDivider;

volatile uint32_t timer16_1_counter = 0;

//extern const float WaveGain[];
extern uint8_t isFindedHome;


extern int content_playing_flag;

extern int	SV_HAND_MAX;
extern int mode;

void TIMER16_0_IRQHandler(void)
{
  LPC_TMR16B0->IR |= 0x01;			/* clear interrupt flag */
  return;
}

void TIMER16_1_IRQHandler(void)
{
	  LPC_TMR16B1->IR |= 0x01;			/* clear interrupt flag */
	return;
}

void enable_timer16(uint8_t timer_num)
{
  if ( timer_num == 0 )
  {
    LPC_TMR16B0->TCR = 1;
  }
  else
  {
    LPC_TMR16B1->TCR = 1;
  }
  return;
}

void disable_timer16(uint8_t timer_num)
{
  if ( timer_num == 0 )
  {
    LPC_TMR16B0->TCR = 0;
  }
  else
  {
    LPC_TMR16B1->TCR = 0;
  }
  return;
}

void reset_timer16(uint8_t timer_num)
{
  uint32_t regVal;

  if ( timer_num == 0 )
  {
    regVal = LPC_TMR16B0->TCR;
    regVal |= 0x02;
    LPC_TMR16B0->TCR = regVal;
  }
  else
  {
    regVal = LPC_TMR16B1->TCR;
    regVal |= 0x02;
    LPC_TMR16B1->TCR = regVal;
  }
  return;
}

void init_timer16B0(uint16_t TimerInterval)
{
    /* Some of the I/O pins need to be clearfully planned if
    you use below module because JTAG and TIMER CAP/MAT pins are muxed. */
    LPC_SYSCON->SYSAHBCLKCTRL |= (1<<7);

    LPC_TMR16B0->EMR = (1<<EMC3)|(1<<EMC2)|(1<<EMC1)|(2<<EMC0)|(1<<3)|(MATCH0|MATCH1);

	LPC_IOCON->PIO0_8           &= ~0x07;
	LPC_IOCON->PIO0_8           |= 0x02;		/// Timer1_16 MAT0
	LPC_IOCON->PIO0_9          &= ~0x07;
	LPC_IOCON->PIO0_9          |= 0x02;		/// Timer1_16 MAT1

	/* Enable the selected PWMs and enable Match3 */
	LPC_TMR16B0->PWMC = (1<<3)|(MATCH0|MATCH1);
    //timer16_0_counter = 0;

    LPC_TMR16B0->PR  = 3; /* set prescaler to get 1 M counts/sec */

	LPC_TMR16B0->MR0 = TimerInterval/2;
	LPC_TMR16B0->MR1 = TimerInterval/2;
	LPC_TMR16B0->MR3 = TimerInterval + 1;

    LPC_TMR16B0->MCR = (1<<10);				/* Interrupt and Reset on MR3 */

    /* Enable the TIMER0 Interrupt */
    //NVIC_EnableIRQ(TIMER_16_0_IRQn);
}

void init_timer16B1(uint16_t TimerInterval)
{

	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<8);

	LPC_TMR16B1->EMR = (1<<EMC3)|(1<<EMC2)|(1<<EMC1)|(2<<EMC0)|(1<<3)|(MATCH0|MATCH1);

	LPC_IOCON->PIO1_9           &= ~0x07;
	LPC_IOCON->PIO1_9           |= 0x01;
	LPC_IOCON->PIO1_10          &= ~0x07;
	LPC_IOCON->PIO1_10          |= 0x02;

	/* Enable the selected PWMs and enable Match3 */
	LPC_TMR16B1->PWMC = (1<<3)|(MATCH0|MATCH1);

	//200khz
	LPC_TMR16B1->PR = 3;	//2MHz

	LPC_TMR16B1->MR0 = TimerInterval/2;
	LPC_TMR16B1->MR1 = TimerInterval/2;
	LPC_TMR16B1->MR3 = TimerInterval + 1;

	LPC_TMR16B1->MCR = (1<<10);				/* Interrupt and Reset on MR3 */

	/* Enable the TIMER1 Interrupt */
	//NVIC_EnableIRQ(TIMER_16_1_IRQn);

}
/******************************************************************************
** Function name:		pwm16_setMatch
**
** Descriptions:		Set the pwm16 match values
**
** parameters:			timer number, match numner and the value
**
** Returned value:		None
**
******************************************************************************/
void setMatch_timer16PWM (uint8_t timer_num, uint8_t match_nr, uint32_t value)
{
	if (timer_num)
	{
		switch (match_nr)
		{
			case 0:
				LPC_TMR16B1->MR0 = value;
				break;
			case 1:
				LPC_TMR16B1->MR1 = value;
				break;
			case 2:
				LPC_TMR16B1->MR2 = value;
				break;
			case 3:
				LPC_TMR16B1->MR3 = value;
				break;
			default:
				break;
		}
	}
	else
	{
		switch (match_nr)
		{
			case 0:
				LPC_TMR16B0->MR0 = value;
				break;
			case 1:
				LPC_TMR16B0->MR1 = value;
				break;
			case 2:
				LPC_TMR16B0->MR2 = value;
				break;
			case 3:
				LPC_TMR16B0->MR3 = value;
				break;
			default:
				break;
		}
	}

}


/******************************************************************************
**                            End Of File
******************************************************************************/
