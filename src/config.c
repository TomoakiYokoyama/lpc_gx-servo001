/*
 * config.c
 *
 *  Created on: 2014/10/01
 *      Author: yokoyama
 */
#include "LPC11xx.h"
#include "uart.h"
#include "memmap.h"
#include "flash.h"
#include "ports.h"
#include "gpio.h"
#include "config.h"
#include "servo.h"
#include "adc.h"

volatile uint32_t syscnt;
volatile uint32_t syscnt_old;

const uint64_t BoudRateList[] = {
		9600,	//00H
		14400,	//01H
		19200,	//02H
		28800,	//03H
		38400,	//04H
		57600,	//05H
		76800,	//06H
		115200,	//07H
		153600,	//08H
		230400,	//09H
};

void SVCall_Handler(void)
{
	proc_Rs30x();
}

void init(){
	int i;
	syscnt = 0;
	syscnt_old = 0;
	GPIOInit();
	GPIOSetDir(P_TxD,GPIO_INPUT);
	GPIOSetDir(P_RxD,GPIO_INPUT);

	FLASH_READ_MEMMAP();
	if(memmap[0].map.ProductID != MEMMAP_PRODUCTID_DEF){
		InitMemmapAll();
		FLASH_WRITE_MEMMAP();
	}


	//NVIC_SetPriority (UART_IRQn, 3);
	//NVIC_SetPriority (SVCall_IRQn, 2);

	{
		if(memmap[0].map.Baud_Rate < sizeof(BoudRateList)){
			UARTInit(BoudRateList[memmap[0].map.Baud_Rate]);
		}
		else{
			memmap[0].map.Baud_Rate = 7;
			UARTInit(115200);
		}
	}
	//NVIC_EnableIRQ(SVCall_IRQn);

	ADCInit( ADC_CLK );
	initServo();

	SysTick_Config(SystemCoreClock / MAIN_CYCLE);			// Configuration

}

void Sync(){
	while(syscnt == syscnt_old){
		//__NOP();
		proc_Rs30x();
	}
	syscnt_old = syscnt;
}


void SysTick_Handler(void)
{
	syscnt++;
}

void ADCProc(){
	short advalue = 0;
	advalue = ADCRead(0);
	if(memmap[0].map.Reverse == 0){
		memmap[0].map.Present_Posion = (advalue - AD_CENTER) * CONV_AD_2_DEG ;
	}
	else{
		memmap[0].map.Present_Posion = (advalue - AD_CENTER) * -CONV_AD_2_DEG ;
	}

	advalue = ADCRead(1);
	if(memmap[0].map.Reverse == 0){
		memmap[1].map.Present_Posion = (advalue - AD_CENTER) * CONV_AD_2_DEG ;
	}
	else{
		memmap[1].map.Present_Posion = (advalue - AD_CENTER) * -CONV_AD_2_DEG ;
	}

	advalue = ADCRead(2);
	if(memmap[0].map.Reverse == 0){
		memmap[2].map.Present_Posion = (advalue - AD_CENTER) * CONV_AD_2_DEG ;
	}
	else{
		memmap[2].map.Present_Posion = (advalue - AD_CENTER) * -CONV_AD_2_DEG ;
	}

	advalue = ADCRead(3);
	if(memmap[0].map.Reverse == 0){
		memmap[3].map.Present_Posion = (advalue- AD_CENTER) * CONV_AD_2_DEG ;
	}
	else{
		memmap[3].map.Present_Posion = (advalue - AD_CENTER) * -CONV_AD_2_DEG ;
	}

	advalue = ADCRead(5);
	{
		double volt = (double)advalue;
		volt *= 330*3.0/1024;
		memmap[0].map.Present_Volts = (uint16_t)volt;
		memmap[1].map.Present_Volts = (uint16_t)volt;
		memmap[2].map.Present_Volts = (uint16_t)volt;
		memmap[3].map.Present_Volts = (uint16_t)volt;
	}
}
