/*****************************************************************************
 *   uarttest.c:  UART test C file for NXP LPC11xx Family Microprocessors
 *
 *   Copyright(C) 2008, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.12.07  ver 1.00    Preliminary version, first Release
 *
******************************************************************************/
#include "LPC11xx.h"
#include "uart.h"
#include "rs30x.h"
#include "adc.h"
#include "memmap.h"

int main (void) {
	int i,cnt = 0;
	init();
	SetUartRx();

	while (1)
	{
		Sync();
		ADCProc();
		ServoProc();
		for(i = 0 ; i < 4;i++){
			if(memmap[i].map.Reverse > 1){
				memmap[i].map.Reverse = 1;
			}
		}

#if 0
		//ecoh back
		if ( oldUARTCount !=  UARTCount)
		{
			//LPC_UART->IER = IER_THRE | IER_RLS;

			SetUartTx();

			if(oldUARTCount < UARTCount){
				UARTSend( (uint8_t *)(&UARTBuffer[oldUARTCount]), UARTCount - oldUARTCount );
			}
			else{
				UARTSend( (uint8_t *)(&UARTBuffer[oldUARTCount]), sizeof(UARTBuffer) - oldUARTCount);
				UARTSend( (uint8_t *)(UARTBuffer), UARTCount);
			}
			oldUARTCount = UARTCount;
			SetUartRx();
		}
#endif
	}
}
