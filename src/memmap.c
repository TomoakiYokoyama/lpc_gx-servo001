/*
 * memmap.c
 *
 *  Created on: 2014/10/01
 *      Author: yokoyama
 */


#include "memmap.h"


volatile MEMMAP_UNION memmap[4];

int getServoNum(uint8_t id){
	int i;
	for(i = 0; i < 4;i++){
		if(id == memmap[i].map.Servo_ID){
			return i;
		}
	}
	return -1;
}

void InitMemmapAll(){
	int i;
	for(i = 0; i < 4;i++){
		InitMemmap(i);
	}
}

void InitMemmapId(int id){
	int servonum = getServoNum(id);
	if(servonum >= 0){
		InitMemmap(servonum);
	}
}

void InitMemmap(int i){
	memmap[i].map.ProductID = MEMMAP_PRODUCTID_DEF + (i*0x1000);

	memmap[i].map.Version = 0x0001;
	memmap[i].map.Servo_ID = i+12;

	memmap[i].map.Reverse = 0x00;
	memmap[i].map.Baud_Rate = 0x07;
	memmap[i].map.Return_Delay = 0x00;
	memmap[i].map.CW_Angle_Limit = 900;
	memmap[i].map.CCW_Angle_Limit = -900;
	memmap[i].map.Reserved1 = 0x00;
	memmap[i].map.Reserved2 = 0x00;
	memmap[i].map.Temperature_Limit = 77;
	memmap[i].map.Reserved3 = 0x00;
	memmap[i].map.Reserved4 = 0x00;
	memmap[i].map.Reserved5 = 0x00;
	memmap[i].map.Reserved6 = 0x00;
	memmap[i].map.Reserved7 = 0x00;
	memmap[i].map.Reserved8 = 0x00;
	memmap[i].map.Torque_in_Silence = 0x00;
	memmap[i].map.Warm_up_Time = 0xC8;
	memmap[i].map.CW_Compliance_Margin = 0x02;
	memmap[i].map.CCW_Compliance_Margin = 0x02;
	memmap[i].map.CW_Compliance_Slope = 20;
	memmap[i].map.CCW_Compliance_Slope = 20;
	memmap[i].map.Punch = 100;
	memmap[i].map.Goal_Position = 0x00;
	memmap[i].map.Goal_Time = 0x00;
	memmap[i].map.Reserved9 = 0x00;
	memmap[i].map.Max_Torque = 0x64;
	memmap[i].map.Torque_Enable = 0x00;
	memmap[i].map.Reserved10 = 0x00;
	memmap[i].map.Reserved11 = 0x00;
	memmap[i].map.Reserved12 = 0x00;
	memmap[i].map.Reserved13 = 0x00;
	memmap[i].map.Reserved14 = 0x00;
	memmap[i].map.Present_Posion = 0x00;
	memmap[i].map.Present_Time = 0x00;
	memmap[i].map.Present_Speed = 0x00;
	memmap[i].map.Present_Current = 0x00;
	memmap[i].map.Present_Temperature = 0x00;
	memmap[i].map.Present_Volts = 0x00;
	memmap[i].map.Reserved15 = 0x00;
	memmap[i].map.Reserved16 = 0x00;
	memmap[i].map.Reserved17 = 0x00;
	memmap[i].map.Reserved18 = 0x00;
	memmap[i].map.Reserved19 = 0x00;
	memmap[i].map.Reserved20 = 0x00;
}
