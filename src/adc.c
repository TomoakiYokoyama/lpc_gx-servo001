/*****************************************************************************
 *   main.c:  Main C file for NXP LPC11xx Family Microprocessors
 *
 *   Copyright(C) 2008, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.12.07  ver 1.00    Preliminary version, first Release
 *
*****************************************************************************/
#include "LPC11xx.h"			/* LPC11xx Peripheral Registers */
#include "adc.h"
#include "memmap.h"
#include "servo.h"

volatile uint32_t POT1;
volatile uint32_t POT2;
volatile uint32_t SW;
volatile uint32_t ADCIntDone = 0;

#if BURST_MODE
volatile uint32_t channel_flag; 
#endif

#if ADC_INTERRUPT_FLAG
/******************************************************************************
** Function name:		ADC_IRQHandler
**
** Descriptions:		ADC interrupt handler
**
** parameters:			None
** Returned value:		None
** 
******************************************************************************/


void ADC_IRQHandler (void) 
{
  uint32_t regVal;
  
  LPC_ADC->CR &= 0xF8FFFFFF;	/* stop ADC now */
  regVal = LPC_ADC->STAT;		/* Read ADC will clear the interrupt */
  if ( regVal & 0x0000FF00 )	/* check OVERRUN error first */
  {
	regVal = (regVal & 0x0000FF00) >> 0x08;
	/* if overrun, just read ADDR to clear */
	/* regVal variable has been reused. */
	switch ( regVal )
	{
	  case 0x01:
		regVal = LPC_ADC->DR[0];
		break;
	  case 0x02:
		regVal = LPC_ADC->DR[1];
		break;
	  case 0x04:
		regVal = LPC_ADC->DR[2];
		break;
	  case 0x08:
		regVal = LPC_ADC->DR[3];
		break;
	  case 0x20:
		regVal = LPC_ADC->DR[5];
		break;
	  default:
		break;
	}
	LPC_ADC->CR &= 0xF8FFFFFF;	/* stop ADC now */ 

	switch ( regVal )
	{
	  case 0x01:
		  ADCRead(0);
		break;
	  case 0x02:
		  ADCRead(1);
		break;
	  case 0x04:
		  ADCRead(2);
		break;
	  case 0x08:
		  ADCRead(3);
		break;
	  case 0x20:
		  ADCRead(5);
		break;
		break;
	}
	return;
  }

  if ( regVal & ADC_ADINT )
  {
	switch ( regVal & 0xFF )	/* check DONE bit */
	{
	  case 0x01:
		  if(memmap[0].map.Reverse == 0){
				memmap[0].map.Present_Posion = ((short)((LPC_ADC->DR[0] >> 6 ) & 0x3FF) - AD_CENTER) * CONV_AD_2_DEG ;
		  }
		  else{
				memmap[0].map.Present_Posion = ((short)((LPC_ADC->DR[0] >> 6 ) & 0x3FF) - AD_CENTER) * -CONV_AD_2_DEG ;
		  }
		  ADCRead(1);
	    break;
	  case 0x02:
		  if(memmap[0].map.Reverse == 0){
				memmap[1].map.Present_Posion = ((short)((LPC_ADC->DR[1] >> 6 ) & 0x3FF) - AD_CENTER) * CONV_AD_2_DEG ;
		  }
		  else{
				memmap[1].map.Present_Posion = ((short)((LPC_ADC->DR[1] >> 6 ) & 0x3FF) - AD_CENTER) * -CONV_AD_2_DEG ;
		  }
		  ADCRead(2);
	    break;
	  case 0x04:
		  if(memmap[0].map.Reverse == 0){
				memmap[2].map.Present_Posion = ((short)((LPC_ADC->DR[2] >> 6 ) & 0x3FF) - AD_CENTER) * CONV_AD_2_DEG ;
		  }
		  else{
				memmap[2].map.Present_Posion = ((short)((LPC_ADC->DR[2] >> 6 ) & 0x3FF) - AD_CENTER) * -CONV_AD_2_DEG ;
		  }
		  ADCRead(3);
	    break;
	  case 0x08:
		  if(memmap[0].map.Reverse == 0){
				memmap[3].map.Present_Posion = ((short)((LPC_ADC->DR[3] >> 6 ) & 0x3FF) - AD_CENTER) * CONV_AD_2_DEG ;
		  }
		  else{
				memmap[3].map.Present_Posion = ((short)((LPC_ADC->DR[3] >> 6 ) & 0x3FF) - AD_CENTER) * -CONV_AD_2_DEG ;
		  }
		  ADCRead(5);
	    break;
	  case 0x20:
	  {
		  double volt = (double)((int)((LPC_ADC->DR[5] >> 6 ) & 0x3FF));
		  volt *= 330*3.0/1024;
		  memmap[0].map.Present_Volts = (uint16_t)volt;
		  memmap[1].map.Present_Volts = (uint16_t)volt;
		  memmap[2].map.Present_Volts = (uint16_t)volt;
		  memmap[3].map.Present_Volts = (uint16_t)volt;
		 ADCIntDone ++;
		 ServoProc();
	  }

	    break;
	  default:
	    break;
	}
  }
  return;
}
#endif

/*****************************************************************************
** Function name:		ADCInit
**
** Descriptions:		initialize ADC channel
**
** parameters:			ADC clock rate
** Returned value:		None
** 
*****************************************************************************/
void ADCInit( uint32_t ADC_Clk )
{
  /* Disable Power down bit to the ADC block. */  
  LPC_SYSCON->PDRUNCFG &= ~(0x1<<4);

  /* Enable AHB clock to the ADC. */
  LPC_SYSCON->SYSAHBCLKCTRL |= (1<<13);

  LPC_IOCON->JTAG_TDI_PIO0_11   = 0x02;	// Select AD0 pin function
  LPC_IOCON->JTAG_TMS_PIO1_0    = 0x02;	// Select AD1 pin function
  LPC_IOCON->JTAG_TDO_PIO1_1  = 0x02;	// Select AD2 pin function
  LPC_IOCON->JTAG_nTRST_PIO1_2   = 0x02;	// Select AD3 pin function
  LPC_IOCON->PIO1_4    			= 0x01;	// Select AD5 pin function

  LPC_ADC->CR = ((SystemCoreClock/LPC_SYSCON->SYSAHBCLKDIV)/ADC_Clk-1)<<8;

  /* If POLLING, no need to do the following */
#if ADC_INTERRUPT_FLAG
  NVIC_EnableIRQ(ADC_IRQn);
  LPC_ADC->INTEN = 0x12F;		/* Enable AD0,AD1,AD2,AD3,AD5 interrupts */
#endif
  return;
}

/*****************************************************************************
** Function name:		ADCRead
**
** Descriptions:		Read ADC channel
**
** parameters:			Channel number
** Returned value:		Value read, if interrupt driven, return channel #
** 
*****************************************************************************/
uint32_t ADCRead( uint8_t channelNum )
{
#if !ADC_INTERRUPT_FLAG
  uint32_t regVal, ADC_Data;
#endif

  /* channel number is 0 through 7 */
  if ( channelNum != 0 && channelNum != 1 && channelNum != 2 && channelNum != 3 && channelNum != 5 )
  {
	channelNum = 0;		/* reset channel number to 0 */
  }
  LPC_ADC->CR &= 0xFFFFFF00; // clear channel selection
  LPC_ADC->CR |= (1 << 24) | (1 << channelNum);	

				/* switch channel,start A/D convert */
#if !ADC_INTERRUPT_FLAG
  while ( 1 )			/* wait until end of A/D convert */
  {
	regVal = *(volatile unsigned long *)(LPC_ADC_BASE + ADC_OFFSET + ADC_INDEX * channelNum);
	/* read result of A/D conversion */
	if ( regVal & ADC_DONE )
	{
	  break;
	}
  }	
        
  LPC_ADC->CR &= 0xF8FFFFFF;	/* stop ADC now */    
  if ( regVal & ADC_OVERRUN )	/* save data when it's not overrun, otherwise, return zero */
  {
	return ( 0 );
  }
  ADC_Data = ( regVal >> 6 ) & 0x3FF;
  return ( ADC_Data );	/* return A/D conversion value */
#else
  return ( channelNum );	/* if it's interrupt driven, the ADC reading is 
					done inside the handler. so, return channel number */
#endif
}



