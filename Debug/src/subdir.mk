################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/adc.c \
../src/config.c \
../src/cr_startup_lpc11.c \
../src/flash.c \
../src/gpio.c \
../src/main.c \
../src/memmap.c \
../src/mot.c \
../src/rs30x.c \
../src/servo.c \
../src/timer16.c \
../src/uart.c 

OBJS += \
./src/adc.o \
./src/config.o \
./src/cr_startup_lpc11.o \
./src/flash.o \
./src/gpio.o \
./src/main.o \
./src/memmap.o \
./src/mot.o \
./src/rs30x.o \
./src/servo.o \
./src/timer16.o \
./src/uart.o 

C_DEPS += \
./src/adc.d \
./src/config.d \
./src/cr_startup_lpc11.d \
./src/flash.d \
./src/gpio.d \
./src/main.d \
./src/memmap.d \
./src/mot.d \
./src/rs30x.d \
./src/servo.d \
./src/timer16.d \
./src/uart.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__USE_CMSIS=CMSISv1p30_LPC11xx -DDEBUG -D__CODE_RED -D__REDLIB__ -I"C:\Users\Tomoaki Yokoyama\Documents\LPCXpresso_8.2.2_650\ws_lpc111x\CMSISv1p30_LPC11xx\inc" -I"C:/Users/Tomoaki Yokoyama/Documents/LPCXpresso_8.2.2_650/ws_lpc111x/gx-servo001/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m0 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


