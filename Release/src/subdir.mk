################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/cr_startup_lpc11.c \
../src/gpio.c \
../src/spi_flash.c \
../src/ssp.c \
../src/uart.c \
../src/uarttest.c 

OBJS += \
./src/cr_startup_lpc11.o \
./src/gpio.o \
./src/spi_flash.o \
./src/ssp.o \
./src/uart.o \
./src/uarttest.o 

C_DEPS += \
./src/cr_startup_lpc11.d \
./src/gpio.d \
./src/spi_flash.d \
./src/ssp.d \
./src/uart.d \
./src/uarttest.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__USE_CMSIS=CMSISv1p30_LPC11xx -DNDEBUG -D__CODE_RED -D__REDLIB__ -I"C:\Users\yokoyama_S9\Documents\lpcxpresso_3.5\lpc1114_workspace\CMSISv1p30_LPC11xx\inc" -O2 -Os -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


