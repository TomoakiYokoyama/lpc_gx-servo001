/*
 * config.h
 *
 *  Created on: 2014/10/02
 *      Author: yokoyama
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#define MAIN_CYCLE (100)
//#define ADC_CYCLE (100)
void ADCProc();


#define AD_CENTER (512)
#define CONV_AD_2_DEG (-1000.0/512.0)	//900.0/512

#endif /* CONFIG_H_ */
