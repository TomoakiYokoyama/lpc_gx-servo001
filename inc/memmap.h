/*
 * memmap.h
 *
 *  Created on: 2014/10/01
 *      Author: yokoyama
 */

#ifndef MEMMAP_H_
#define MEMMAP_H_

#include <stdint.h>

#define MEMMAP_SIZE (64)
typedef union
{
	struct{
		unsigned short ProductID;
		unsigned short Version;
		unsigned char Servo_ID;
		unsigned char Reverse;
		unsigned char Baud_Rate;
		unsigned char Return_Delay;
		short CW_Angle_Limit;
		short CCW_Angle_Limit;
		unsigned char Reserved1;
		unsigned char Reserved2;
		unsigned short Temperature_Limit;
		unsigned char Reserved3;
		unsigned char Reserved4;
		unsigned char Reserved5;
		unsigned char Reserved6;
		unsigned char Reserved7;
		unsigned char Reserved8;
		unsigned char Torque_in_Silence;
		unsigned char Warm_up_Time;
		unsigned char CW_Compliance_Margin;
		unsigned char CCW_Compliance_Margin;
		unsigned char CW_Compliance_Slope;
		unsigned char CCW_Compliance_Slope;
		unsigned short Punch;
		short Goal_Position;
		unsigned short Goal_Time;
		unsigned char Reserved9;
		unsigned char Max_Torque;
		unsigned char Torque_Enable;
		unsigned char Reserved10;
		unsigned char Reserved11;
		unsigned char Reserved12;
		unsigned char Reserved13;
		unsigned char Reserved14;
		short Present_Posion;
		unsigned short Present_Time;
		short Present_Speed;
		unsigned short Present_Current;
		unsigned short Present_Temperature;
		unsigned short Present_Volts;
		unsigned char Reserved15;
		unsigned char Reserved16;
		unsigned char Reserved17;
		unsigned char Reserved18;
		unsigned char Reserved19;
		unsigned char Reserved20;
	} map;
	uint8_t u8[MEMMAP_SIZE];
	uint16_t u16[MEMMAP_SIZE/sizeof(uint16_t)];
	uint32_t u32[MEMMAP_SIZE/sizeof(uint32_t)];
	uint64_t u64[MEMMAP_SIZE/sizeof(uint64_t)];
	int8_t s8[MEMMAP_SIZE];
	int16_t s16[MEMMAP_SIZE/sizeof(int16_t)];
	int32_t s32[MEMMAP_SIZE/sizeof(int32_t)];
	int64_t s64[MEMMAP_SIZE/sizeof(int64_t)];
} MEMMAP_UNION;

extern volatile MEMMAP_UNION memmap[4];
extern volatile const MEMMAP_UNION constmemmap[4];

#define MEMMAP_PRODUCTID_DEF (0xC101)

void InitMemmapAll();
void InitMemmapId(int id);
void InitMemmap(int i);
int getServoNum(uint8_t id);

#endif /* MEMMAP_H_ */
