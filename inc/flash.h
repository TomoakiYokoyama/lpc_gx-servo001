#ifndef __FLASH_H__
#define __FLASH_H__

#define	CLOCK_KHz	48000

#define	PAGE_SIZE			256
#define	PAGE_OFFSET_MASK	(PAGE_SIZE-1)
#define	PAGE_SHIFT			8
#define	PAGE_MASK			0x000000FF

#define	SECTOR_SIZE			4096
#define	SECTOR_SHIFT		12
#define	SECTOR_MASK			0x000000F

#define	IAP_PREPARE			50
#define	IAP_RAM2FLASH		51
#define	IAP_ERASE			52


void FLASH_WRITE_MEMMAP();
void FLASH_READ_MEMMAP();
void FLASH_WRITE_MEMMAP_ID(uint8_t id);

extern int srcbuf[PAGE_SIZE/sizeof(int)];

#endif


