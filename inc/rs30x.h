/*
 * rs30x.h
 *
 *  Created on: 2014/10/01
 *      Author: yokoyama
 */

#ifndef RS30X_H_
#define RS30X_H_

typedef union {
	struct{
		unsigned char Header_H;
		unsigned char Header_L;
		unsigned char ID;
		unsigned char Flag;
		unsigned char Address;
		unsigned char Length;
		unsigned char Count;
		unsigned char Data[64];
	}pack;
	uint8_t u8[71];
}RS30xPACKS;

typedef enum{
	RS30xRCV_Header_H = (1),
	RS30xRCV_Header_L = (2),
	RS30xRCV_ID = (3),
	RS30xRCV_Flag = (4),
	RS30xRCV_Address = (5),
	RS30xRCV_Length = (6),
	RS30xRCV_Count = (7),
}RS30xPACK;

#define RS30xFLAG_FLASH (0x40)
#define RS30xFLAG_REBOOT (0x20)
#define RS30xFLAG_INIT (0x10)
#define RS30xFLAG_RET_MASK (0x0F)
#define RS30xFLAG_RET_ACK (0x01)
#define RS30xFLAG_RET_00to29 (0x03)
#define RS30xFLAG_RET_30to59 (0x05)
#define RS30xFLAG_RET_20to29 (0x07)
#define RS30xFLAG_RET_42to59 (0x09)
#define RS30xFLAG_RET_30to41 (0x0B)
#define RS30xFLAG_RET_60to127 (0x0D)
#define RS30xFLAG_RET_SET_ADDR (0x0F)
#define RS30xFLAG_FLASH (0x40)


void proc_Rs30x();
void SetUartTx();
void SetUartRx();
void Rs30xWriteProc();
void Rs30xWriteMem(uint8_t servonum,uint8_t addr , uint8_t size,uint8_t* data);
void Rs30xReturnPack(uint8_t id,uint8_t addr , uint8_t size);

#endif /* RS30X_H_ */
