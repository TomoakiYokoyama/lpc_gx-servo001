#ifndef __PORTS_H__
#define __PORTS_H__

#define P_LED2 (0x20)
#define P_RESET (0x00)
#define P_BOOT (0x01)

#define P_LED_1 (0x18)
#define P_MTR12_MODE (0x02)
#define P_MTR34_MODE (0x03)
#define P_SCL (0x04)
#define P_SDA (0x05)
#define P_MTR3_PWM (0x19)
#define P_MTR1_DIO (0x34)
#define P_MTR2_DIO (0x35)
#define P_MTR3_DIO (0x06)
#define P_MTR4_DIO (0x07)
#define P_MTR1_PWM (0x08)
#define P_MTR2_PWM (0x09)
#define P_LPC-LINK (0x0A)
#define P_MTR4_PWM (0x1A)
#define P_POT1 (0x0B)
#define P_POT2 (0x10)
#define P_POT3 (0x11)
#define P_POT4 (0x12)
#define P_LPC-LINK (0x13)
#define P_VBAT (0x14)

#define P_RxD (0x16)
#define P_TxD (0x17)


#endif
